import pygame as pg

from game.config import FontDefaults, Colors

class Label:
    def __init__(self, text, x, y, 
                 font=FontDefaults.FONT_FAMILY, 
                 font_size=FontDefaults.FONT_SIZE, 
                 color=Colors.WHITE):
    
        self.font = pg.font.SysFont(font, font_size)

        self.image = self.font.render(text, 1, color)
        _, _, w, h = self.image.get_rect()
        self.rect = pg.Rect(x, y, w, h)
        self.text = text
        self.color = color
 
    def set_text(self, text):
        self.image = self.font.render(text, 1, self.color)
 
    def set_font(self, font, size):
        self.font = pg.font.SysFont(font, size)
        self.set_text(self.text, self.color)
 
    def draw(self, screen):
        screen.blit(self.image, self.rect)

        
class GUI:
    def __init__(self):
        self.fps_label = Label('', 10, 10)

    def draw(self, screen):
        self.fps_label.draw(screen)
