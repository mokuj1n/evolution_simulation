import pygame as pg
import numpy as np
from game.config import Colors, Screen, HostDefaults, FoodDefaults
from game.helpers import limit
from threading import Thread
import math
import time

class Host(pg.sprite.Sprite):
    def __init__(self, x, y, food, color=Colors.GREEN):
        super().__init__()

        self.pos = np.array([x, y], dtype='float64')
        self.vel = np.array([HostDefaults.MAX_VEL, HostDefaults.MAX_VEL], dtype='float64')
        self.acc = np.array([0, 0], dtype='float64')
        
        self.color = color
        self.size = HostDefaults.SIZE
        self.energy = HostDefaults.ENERGY

        self.image = pg.Surface([self.size * 2, self.size * 2])
        self.image.fill(Colors.BGCOLOR)
        pg.draw.circle(
            self.image, self.color, (self.size, self.size), self.size)
        self.rect = self.image.get_rect()
        
        self.food = food
        self.perception_radius = 100


    def update(self):
        self.vel += self.acc
        self.vel = limit(self.vel, HostDefaults.MAX_VEL)
        self.pos += self.vel
        self.acc *= 0

        # x, y = pg.mouse.get_pos()
        # mpos = np.array([x, y], dtype='float64')
        
        self.check_boundaries()
        self.eat(self.food)

        self.move()


    def move(self):
        self.rect.x = self.pos[0]
        self.rect.y = self.pos[1]

    def eat(self, items):
        closest = None
        closest_distance = max(Screen.WIDTH, Screen.HEIGHT)
        
        for item in items:
            distance = math.hypot(
                self.pos[0] - item.pos[0], self.pos[1] - item.pos[1])

            if distance < self.size:
                item.kill()
                self.energy += 10

            if distance < closest_distance:
                closest = item
                closest_distance = distance

        if closest is not None and closest_distance < self.perception_radius:
            self.seek(closest.pos)


    def seek(self, target):
        dv = np.add(target, -self.pos)
        sf = np.add(dv, -self.vel)
        self.apply_force(sf)

    def check_boundaries(self):
        desired = None
        if self.pos[0] < Screen.BOUNDARY_SIZE:
            desired = np.array([HostDefaults.MAX_VEL, self.vel[1]])
        elif self.pos[0] > Screen.WIDTH - Screen.BOUNDARY_SIZE:
            desired = np.array([-HostDefaults.MAX_VEL, self.vel[1]])
        if self.pos[1] < Screen.BOUNDARY_SIZE:
            desired = np.array([self.vel[0], HostDefaults.MAX_VEL])
        elif self.pos[1] > Screen.HEIGHT - Screen.BOUNDARY_SIZE:
            desired = np.array([self.vel[0], -HostDefaults.MAX_VEL])
        if desired is not None:
            steer = desired - self.vel
            self.apply_force(steer)

    def apply_force(self, force):
        self.acc += force


class Food(pg.sprite.Sprite):
    def __init__(self, x, y, color=Colors.LIGHT_GREY):
        super().__init__()

        self.pos = np.array([x, y], dtype='float64')
        
        self.color = color
        self.size = FoodDefaults.SIZE

        self.image = pg.Surface([self.size * 2, self.size * 2])
        self.image.fill(Colors.BGCOLOR)
        pg.draw.circle(
            self.image, self.color, (self.size, self.size), self.size)
        self.rect = self.image.get_rect()

        self.rect.x = self.pos[0]
        self.rect.y = self.pos[1]
