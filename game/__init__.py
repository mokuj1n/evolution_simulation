import os.path

__author__ = "Vadim Trifonov"
__copyright__ = "Copyright 2020-2022 Vadim Trifonov"
__license__ = "MIT"
__maintainer__ = __author__
__email__ = "retroarefobia@gmail.com"
__version__ = "0.1.4"
__version_info__ = tuple(int(part) for part in __version__.split('.'))
__description__ = "Simulation of natural selection."
